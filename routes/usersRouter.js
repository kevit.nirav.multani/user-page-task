const express = require('express')
const bcrypt = require('bcrypt')
const sharp = require('sharp')
const fs = require('fs')
const { userModel, validateUser } = require('../models/user.model')
const auth = require('../middleware/auth')
const upload = require('../middleware/upload')
const path = require('path')
const router = express.Router()

//create user
router.post('/create', async (req, res) => {
  console.log(req.body)
  console.log('this is create new user')
  //validate the data of body
  const { error } = validateUser(req.body)
  if (error) {
    console.error(error)
    return res.status(400).send('invalid data format')
  }


  try {
    const user = new userModel(req.body)
    user.password = await bcrypt.hash(user.password, 10)
    await user.save()

    const token = user.generateAuthToken()


    res.header('Authorization', token).send({
      _id: user._id,
      name: user.name,
      email: user.email,
      token: user.token
    })

  } catch (e) {
    console.log(e)
    res.status(500).send('error occured, email might be used already')
  }
})

//sign in user
router.post('/login', async (req, res) => {
  try {
    console.log(req.body)
    const user = await userModel.findByCredentials(req.body.email, req.body.password)
    const token = await user.generateAuthToken()
    res.send({ user: user.name, token })
  } catch (e) {
    console.log(e)
    res.status(400).send()
  }
})

//sign out user
router.post('/logout', auth, async (req, res) => {
  try {
    req.user.token = undefined
    await req.user.save()

    res.send({ success: 'logout' })
  } catch (e) {
    res.status(500).send()
  }
})

//delete user 
router.delete('/', auth, async (req, res) => {
  try {

    //remove img from uploads folder
    const imagePath = path.join(__dirname, `../uploads/${req.user._id.toString()}.png`)
    if (fs.existsSync(imagePath)) {//file exists
      fs.unlink((imagePath), (err) => {
        if (err) throw err
      })
    }

    await req.user.remove()
    res.send()
  } catch (e) {
    console.error(e)
    res.status(500).send()
  }
})

//retrive user data 
router.get('/current', auth, async (req, res) => {
  try {
    const user = await userModel.findById(req.user._id)
    res.send(user)
  } catch (e) {
    res.status(404).send('no data found')
  }
})

//retrive other user's data
router.get('/:id', auth, async (req, res) => {
  try {

    const _id = req.params.id
    const user = await userModel.findById(_id)

    if (!user) {
      throw new Error('user with this id does not exist (retrive other user data)')
    }

    if (req.user._id.toString() === user._id.toString()) {
      return res.send(user)
    }

    res.send({
      name: user.name,
      avatar: `${process.env.URL}` + user.avatar
    })
  }
  catch (e) {
    console.log(e)
    res.status(404).send('error')
  }

})

//update user
router.patch('/', auth, async (req, res) => {
  const updates = Object.keys(req.body)
  const allowedUpdates = ['name', 'email', 'password', 'phone']

  const isValidOperation = updates.every(update => allowedUpdates.includes(update))

  if (!isValidOperation) {
    return res.status(400).send({ error: 'invalid updates' })
  }

  try {
    updates.forEach(update => req.user[update] = req.body[update])

    await req.user.save()

    res.send(req.user)
  } catch (e) {
    res.status(400).send(e)
  }
})

//upload profile picture
router.post('/avatar', auth, upload.single('uploads'), async (req, res) => {
  try {
    const buffer = await sharp(req.file.buffer).resize({ width: 200, height: 200 }).png().toBuffer()

    fs.writeFile(path.join(__dirname, `../uploads/${req.user._id.toString()}.png`), buffer, 'binary', (err) => {
      if (err) throw err
    })


    req.user.avatar = `/users/uploads/${req.user._id.toString()}`
    await req.user.save()
    res.send()

  } catch (e) {
    console.error(e)
    return res.status(500).send()
  }


}, (error, req, res, next) => {
  console.error(error)
  res.status(400).send({ error: error.message })
})

//delete user avatar
router.delete('/avatar', auth, async (req, res) => {
  try {
    //remove img from uploads folder
    const imagePath = path.join(__dirname, `../uploads/${req.user._id.toString()}.png`)
    if (fs.existsSync(imagePath)) {
      //file exists
      fs.unlink((imagePath), (err) => {
        if (err) throw err
      })
    }

    req.user.avatar = undefined
    await req.user.save()
    res.send()
  } catch (e) {
    res.status(500).send()
  }
})

//get avatar
router.get('/avatar/:id', auth, async (req, res) => {
  try {
    const _id = req.params.id
    const user = await userModel.findById(_id)

    if (!user) throw new Error('user doesnt exist for that avatar')
    res.send(`${process.env.URL}` + user.avatar)
  }
  catch (e) {
    console.log(e)
    return res.status(404).send('not found')
  }

})

//see avatar by opening link
router.get('/uploads/:id', auth, async (req, res) => {
  try {
    res.set('Content-Type', 'image/png')
    const imagePath = path.join(__dirname, `../uploads/${req.params.id}.png`)
    res.sendFile(imagePath)
  } catch (e) {
    console.error(e)
    res.status(404).send('not found')
  }
})

module.exports = router