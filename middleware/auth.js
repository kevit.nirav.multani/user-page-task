const jwt = require('jsonwebtoken')
const { userModel } = require('../models/user.model')

const auth = async (req, res, next) => {

    try {
    const token = req.header('Authorization').replace('Bearer ', '')

    //no token found
    if (!token) {
        return res.status(401).send('no token found, access denied')
    }

        const decoded = jwt.verify(token, process.env.JWT_KEY)
        const user = await userModel.findOne({ _id: decoded._id })
        if (!user) {
            throw new Error('user does not exists (auth.js)')
        }

        req.user = user
        next()
    } catch (e) {
        res.status(400).send('invalid token')
    }
}

module.exports = auth