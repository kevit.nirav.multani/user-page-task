const multer = require('multer')

const upload = multer({
    limits: 10000000,
    //dest: 'uploads',
    fileFilter(req, file, next) {
        if (!file.originalname.match(/\.(jpeg|jpg)$/)) {
            return next(new Error('please upload a picture with extension only jpg/jpeg'))
        }

        next(undefined, true)
    }
    // },
    // storage: multer.diskStorage({
    //     destination: function (req, file, cb) {
    //         cb(null, 'uploads')
    //     },
    //     filename: function (req, file, cb) {
    //         cb(null, req.user._id.toString()+".jpeg")
    //     }
    // })
})

module.exports = upload
