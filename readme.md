### precaution  



- Create a new .`env` file and copy all variables from `example.env` file into this file and make changes accordingly.

- make sure to create folder named 'uploads' in root of the project, it's where uploaded images would be saved



### postman guide 

create env variable named `userPageAuthToken` and `url` (url with initial value of hosted url) in postman  

if using postman then put following in tests tab 

```
if(pm.response.code === 200){
	pm.environment.set( 'userPageAuthToken' , pm.response.json().token)  
}    
```

​      

###  routes example  



###### create user:

post > {{url}}/users/create  

​		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;name: string  (required)  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email: email   (required)  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;password: 3 to 8 char (required)  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;phone: string of 10digits           



###### login:  

​    post > {{url}}/users/login  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;email  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;password  



###### logout:  

​    post > {{url}}/users/logout  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;token  



###### get user info:  

​    get > {{url}}/users/current  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;token  



###### delete user:

​	delete > {{url}}/users/

​		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;token



###### upload avatar:  

​    post > {{url}}/users/avatar  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;token  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;form-data : image  (keyname: uploads)



###### delete avatar:  

​    delete > {{url}}/users/avatar  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;token  

 

###### update profile:   

​    patch > {{url}}/users/  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;token  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;update data (name, email, phone, password)  



###### find other user:  

​    get > {{url}}/users/id  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;token  



###### get other's avatar:  

​    get > {{url}}/users/avatar/id  

​        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;token  