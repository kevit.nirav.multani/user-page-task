const mongoose = require('mongoose')
const Joi = require('joi')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 15
    },
    email: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 255,
        unique: true
    },
    password: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 255
    },
    phone: {
        type: Number
    },
    avatar: {
        type: String
    },
    token: {
        type: String
    }
})

//custom method to generate authToken 
userSchema.methods.generateAuthToken = async function () {
    const user = this
    const token = jwt.sign({ _id: user._id }, process.env.JWT_KEY);
    user.token = token
    await user.save()

    return token;
}

//delete value not to be exposed
userSchema.methods.toJSON = function () {
    const user = this

    //edit avatar link
    const currentAvatarValue = user.avatar
    user.avatar = `${process.env.URL}` + currentAvatarValue

    const userObject = user.toObject()
    if (user.avatar === `${process.env.URL}undefined`) {
        delete userObject.avatar
    }

    delete userObject.password
    delete userObject.token
    delete userObject.__v
    return userObject
}

//check user credentials for login
userSchema.statics.findByCredentials = async (email, password) => {
    const user = await userModel.findOne({ email })

    if (!user) {
        throw new Error("unable to login")

    }

    const isMatch = await bcrypt.compare(password, user.password)

    if (!isMatch) {
        throw new Error("unable to login")
    }

    return user
}

const userModel = mongoose.model('users', userSchema)

//function to validate user 
function validateUser(user) {
    const schema = {
        name: Joi.string().min(3).max(15).required(),
        email: Joi.string().min(5).max(255).required().email(),
        password: Joi.string().min(3).max(8).required(),
        phone: Joi.string().regex(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/)
    }

    return Joi.validate(user, schema);
}

module.exports = {
    userModel,
    validateUser
}
