const mongoose = require('mongoose')


mongoose.connect(process.env.LOCAL_DB_PATH, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false 
})